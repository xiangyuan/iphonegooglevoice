//
//  AppDelegate.h
//  ZFBuyPhone
//
//  Created by li yajie on 12-7-31.
//  Copyright (c) 2012年 com.loongjoy.iPhoneFish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
