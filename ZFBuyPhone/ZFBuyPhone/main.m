//
//  main.m
//  ZFBuyPhone
//
//  Created by li yajie on 12-7-31.
//  Copyright (c) 2012年 com.loongjoy.iPhoneFish. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
