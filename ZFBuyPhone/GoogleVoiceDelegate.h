//
//  GoogleVoiceDelegate.h
//  MMSpeak
//
//  Created by li yajie on 12-7-27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GoogleVoiceDelegate <NSObject>

@required
/**
 * update the data to google voice server
 */
-(void) uploadAudioToGoogle;

@end
